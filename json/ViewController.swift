//
//  ViewController.swift
//  json
//
//  Created by ADMIN on 23/09/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
            let json2 = """
               {
            "count": 1118,
            "next": "https://pokeapi.co/api/v2/pokemon/?offset=20&limit=20",
            "previous": null,
            "results": [
                {
                    "name": "bulbasaur",
                    "url": "https://pokeapi.co/api/v2/pokemon/1/"
                },
                {
                    "name": "ivysaur",
                    "url": "https://pokeapi.co/api/v2/pokemon/2/"
                },
                {
                    "name": "venusaur",
                    "url": "https://pokeapi.co/api/v2/pokemon/3/"
                },
                {
                    "name": "charmander",
                    "url": "https://pokeapi.co/api/v2/pokemon/4/"
                },
                {
                    "name": "charmeleon",
                    "url": "https://pokeapi.co/api/v2/pokemon/5/"
                },
                {
                    "name": "charizard",
                    "url": "https://pokeapi.co/api/v2/pokemon/6/"
                },
                {
                    "name": "squirtle",
                    "url": "https://pokeapi.co/api/v2/pokemon/7/"
                },
                {
                    "name": "wartortle",
                    "url": "https://pokeapi.co/api/v2/pokemon/8/"
                },
                {
                    "name": "blastoise",
                    "url": "https://pokeapi.co/api/v2/pokemon/9/"
                },
                {
                    "name": "caterpie",
                    "url": "https://pokeapi.co/api/v2/pokemon/10/"
                },
                {
                    "name": "metapod",
                    "url": "https://pokeapi.co/api/v2/pokemon/11/"
                },
                {
                    "name": "butterfree",
                    "url": "https://pokeapi.co/api/v2/pokemon/12/"
                },
                {
                    "name": "weedle",
                    "url": "https://pokeapi.co/api/v2/pokemon/13/"
                },
                {
                    "name": "kakuna",
                    "url": "https://pokeapi.co/api/v2/pokemon/14/"
                },
                {
                    "name": "beedrill",
                    "url": "https://pokeapi.co/api/v2/pokemon/15/"
                },
                {
                    "name": "pidgey",
                    "url": "https://pokeapi.co/api/v2/pokemon/16/"
                },
                {
                    "name": "pidgeotto",
                    "url": "https://pokeapi.co/api/v2/pokemon/17/"
                },
                {
                    "name": "pidgeot",
                    "url": "https://pokeapi.co/api/v2/pokemon/18/"
                },
                {
                    "name": "rattata",
                    "url": "https://pokeapi.co/api/v2/pokemon/19/"
                },
                {
                    "name": "raticate",
                    "url": "https://pokeapi.co/api/v2/pokemon/20/"
                }
            ]
            }
            """
            
        func viewDidLoad() {
                super.viewDidLoad()
                let data = json2.data(using: .utf8)
                
                print(data ?? Data())
                
                do {
                    let jsonDecoder = JSONDecoder()
                    let decodedResponse = try jsonDecoder.decode(Response.self,
                                                                 from: data ?? Data())
                    print(decodedResponse)
                } catch {
                    print(error)
                }
            UserDefaults.standard.setValue(<#T##value: Any?##Any?#>, forKey: <#T##String#>)
            
            }
            func getPlist(withName name: String) -> [String]?
                {
                    if  let path = Bundle.main.path(forResource: name, ofType: "plist"),
                        let xml = FileManager.default.contents(atPath: path)
                    {
                        return (try? PropertyListSerialization.propertyList(from: xml, options: .mutableContainersAndLeaves, format: nil)) as? [String]
                    }
                    return nil
                }

            struct  Response: Decodable {
                var count: Int!
                var next: String!
                var previous: String!
                var results: [Object]!
            }

            struct Object: Decodable {
                var name: String!
                var url: String!
            }
        }

        // Do any additional setup after loading the view.
    }




